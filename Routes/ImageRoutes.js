import express from "express";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import multer from "multer";
import UploadImage from "../Models/ImageModel.js";


const imagesRouter = express.Router();

// Storage
const Storage = multer.diskStorage({
  destination: "uploads",
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({
  storage: Storage,
}).single("file");

// Add image
imagesRouter.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    upload(req, res, (err) => {
      if (err) {
        console.log(err);
      } else {
        const newImage = new UploadImage({
          image: {
            date: req.file.filename,
            contentType: "image/png",
          },
        });
        return newImage
          .save()
          .then(() => res.send(req.file.filename))
          .catch((err) => console.log(err));
      }
    });
  })
);

// Get image
imagesRouter.get(
  "/",
  asyncHandler(async (req, res) => {
    const image = await UploadImage.find({});
    res.json(image);
  })
);

//Delete image
imagesRouter.delete(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const image = await UploadImage.findById(req.params.id);
    if (image) {
      await image.remove();
      res.json({ message: "Supprimé" });
    } else {
      res.status(404);
      throw new Error("Image invalide");
    }
  })
);

// Edit image
imagesRouter.put(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { image } = req.body;
    const imageUpload = await UploadImage.findById(req.params.id);
    if (imageUpload) {
      imageUpload.image = image || imageUpload.image;

      const updatedImage = await imageUpload.save();
      res.json(updatedImage);
    } else {
      res.status(404);
      throw new Error("Image non trouvé");
    }
  })
);

export default imagesRouter;
