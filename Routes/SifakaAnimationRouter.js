import express from "express";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import SifakaAnimation from "../Models/SifakaAnimation.js";

const sifakaAnimationRouter = express.Router();

//Create Animation Sifaka
sifakaAnimationRouter.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { animation } = req.body;
    const animationSifaka = new SifakaAnimation({
      animation,
    });
    if (animationSifaka) {
      const createSifakaAnimation = await animationSifaka.save();
      res.status(201).json(createSifakaAnimation);
    } else {
      res.status(400);
      throw new Error("Animation invalide");
    }
  })
);

//Get Animation Sifaka
sifakaAnimationRouter.get(
  "/animation-sifaka",
  protect,
  asyncHandler(async (req, res) => {
    const animationSifaka = await SifakaAnimation.find({});
    // console.log("animationFlashSale", animationFlashSale);
    res.json(animationSifaka);
  })
);

//Get Animation Sifaka Client
sifakaAnimationRouter.get(
  "/animation-sifaka-client",
  asyncHandler(async (req, res) => {
    const animationSifaka = await SifakaAnimation.find({});
    // console.log("animationFlashSale", animationFlashSale);
    res.json(animationSifaka);
  })
);

//Get Single Animation Sifaka
sifakaAnimationRouter.get(
  "/:id",
  asyncHandler(async (req, res) => {
    const singleAnimationSifaka = await SifakaAnimation.findById(req.params.id);
    if (singleAnimationSifaka) {
      res.json(singleAnimationSifaka);
    } else {
      res.status(404);
      throw new Error("Animation Sifaka invalide");
    }
  })
);

//Edit Animation Sifaka
sifakaAnimationRouter.put(
  "/:id",
  asyncHandler(async (req, res) => {
    const { animation } = req.body;
    const editAnimationSifaka = await SifakaAnimation.findById(req.params.id);
    if (editAnimationSifaka) {
      editAnimationSifaka.animation =
        animation || editAnimationSifaka.animation;

      const upDateAnimationSifaka = await editAnimationSifaka.save();
      res.json(upDateAnimationSifaka);
    } else {
      res.status(404);
      throw new Error("Animation Sifaka introuvable");
    }
  })
);

//Delete Animation Sifaka
sifakaAnimationRouter.delete(
  "/:id",
  asyncHandler(async (req, res) => {
    const deleteAnimationSifaka = await SifakaAnimation.findById(req.params.id);
    if (deleteAnimationSifaka) {
      await deleteAnimationSifaka.remove();
      res.json({ message: "Animation Sifaka supprimer" });
    } else {
      res.status(404);
      throw new Error("Animation Sifaka introuvable");
    }
  })
);

export default sifakaAnimationRouter;
