import express from "express";
import slugify from "slugify";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import SubCategory from "../Models/SubCategory.js";

const subCategoryRouter = express.Router();

// Create SubCategory
subCategoryRouter.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const subCategoryObj = {
      name: req.body.name,
    };
    const { name } = req.body;
    const subCategoryExist = await SubCategory.findOne({ name });
    if (subCategoryExist) {
      res.status(404);
      throw new Error("Nom du sous-categorie déjà existe");
    } else {
      if (req.body.parentId) {
        subCategoryObj.parentId = req.body.parentId;
      }

      const subCategory = new SubCategory(subCategoryObj);

      const subCategoryCreate = await subCategory.save();
      res.status(201).json(subCategoryCreate);
    }
  })
);

// Get all category
subCategoryRouter.get(
  "/",
  asyncHandler(async (req, res) => {
    const subCategories = await SubCategory.find({});
    // res.json(categories);
    function createCategories(subCategories, parentId) {
      const subCategoryList = [];
      let subCategory;
      if (!parentId) {
        subCategory = subCategories?.filter(
          (cat) => cat?.parentId === undefined
        );
      } else {
        subCategory = subCategories?.filter(
          (cat) => cat?.parentId === parentId?.toString()
        );
      }

      for (let cate of subCategory) {
        subCategoryList.push({
          _id: cate._id,
          name: cate.name,
          children: createCategories(subCategories, cate._id),
        });
      }

      return subCategoryList;
    }

    if (subCategories) {
      const subCategoryList = createCategories(subCategories);

      res.status(200).json({ subCategoryList });
    }
  })
);

export default subCategoryRouter;
