import express from "express";
import slugify from "slugify";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import Category from "../Models/Category.js";

const categoryRouter = express.Router();

// Create category
categoryRouter.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const categoryObj = {
      name: req.body.name,
      slug: slugify(req.body.name),
      bgColor: req.body.bgColor,
    };
    const { name } = req.body;
    const categoryExist = await Category.findOne({ name });
    if (categoryExist) {
      res.status(404);
      throw new Error("Nom du categorie déjà existe");
    } else {
      if (req.body.parentId) {
        categoryObj.parentId = req.body.parentId;
      }

      const category = new Category(categoryObj);

      const categoryCreate = await category.save();
      res.status(201).json(categoryCreate);
    }
  })
);

// Get all category
categoryRouter.get(
  "/",
  asyncHandler(async (req, res) => {
    const categories = await Category.find({});
    // res.json(categories);
    function createCategories(categories, parentId) {
      const categoryList = [];
      let category;
      if (!parentId) {
        category = categories?.filter((cat) => cat?.parentId === undefined);
      } else {
        category = categories?.filter(
          (cat) => cat?.parentId === parentId?.toString()
        );
      }

      for (let cate of category) {
        categoryList.push({
          _id: cate._id,
          name: cate.name,
          slug: cate.slug,
          bgColor: cate.bgColor,
          children: createCategories(categories, cate._id),
        });
      }

      return categoryList;
    }

    if (categories) {
      const categoryList = createCategories(categories);

      res.status(200).json({ categoryList });
    }
  })
);

// Delet Category
categoryRouter.delete(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const category = await Category.findById(req.params.id);
    if (category) {
      await category.remove();
      res.json({ message: "Categorie supprimé" });
    } else {
      res.status(404);
      throw new Error("Categorie invalide");
    }
  })
);

// Edit Category
categoryRouter.put(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { name, slug, bgColor } = req.body;
    const category = await Category.findById(req.params.id);
    if (category) {
      category.name = name || category.name;
      category.slug = slug || category.slug;
      category.bgColor = bgColor || category.bgColor;

      const updatedCategory = await category.save();
      res.json(updatedCategory);
    } else {
      res.status(404);
      throw new Error("Categorie non trouvé");
    }
  })
);

export default categoryRouter;
