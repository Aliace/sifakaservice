import express from "express";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import FlashSaleAnimation from "../Models/FlashSaleAnimation.js";

const flashSaleAnimationRouter = express.Router();

//Create Animation Flash Sale
flashSaleAnimationRouter.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { animation } = req.body;
    const animationFlashSale = await FlashSaleAnimation.create({
      animation,
    });
    if (animationFlashSale) {
      res.status(201).json({
        animation: animationFlashSale.animation,
      });
    } else {
      res.status(400);
      throw new Error("Animation invalide");
    }
  })
);

//Get Animation Flash Sale Admin
flashSaleAnimationRouter.get(
  "/animation-vente",
  protect,
  asyncHandler(async (req, res) => {
    const animationFlashSale = await FlashSaleAnimation.find({});
    // console.log("animationFlashSale", animationFlashSale);
    res.json(animationFlashSale);
  })
);

//Get Animation Flash Sale
flashSaleAnimationRouter.get(
  "/animation-vente-client",
  asyncHandler(async (req, res) => {
    const animationFlashSale = await FlashSaleAnimation.find({});
    // console.log("animationFlashSale", animationFlashSale);
    res.json(animationFlashSale);
  })
);

//Get Single Animation
flashSaleAnimationRouter.get(
  "/:id",
  asyncHandler(async (req, res) => {
    const singleAnimationFlashSale = await FlashSaleAnimation.findById(
      req.params.id
    );
    if (singleAnimationFlashSale) {
      res.json(singleAnimationFlashSale);
    } else {
      res.status(404);
      throw new Error("Animation Vente flash invalide");
    }
  })
);

//Edit Animation Flash Sale
flashSaleAnimationRouter.put(
  "/:id",
  asyncHandler(async (req, res) => {
    const { animation } = req.body;
    const editAnimationFlashSale = await FlashSaleAnimation.findById(
      req.params.id
    );
    if (editAnimationFlashSale) {
      editAnimationFlashSale.animation =
        animation || editAnimationFlashSale.animation;

      const upDateAnimationFlashSale = await editAnimationFlashSale.save();
      res.json(upDateAnimationFlashSale);
    } else {
      res.status(404);
      throw new Error("Animation Vente flash introuvable");
    }
  })
);

//Delete Animation Flash Sale
flashSaleAnimationRouter.delete(
  "/:id",
  asyncHandler(async (req, res) => {
    const deleteAnimationFlashSale = await FlashSaleAnimation.findById(
      req.params.id
    );
    if (deleteAnimationFlashSale) {
      await deleteAnimationFlashSale.remove();
      res.json({ message: "Animation Vente flash supprimer" });
    } else {
      res.status(404);
      throw new Error("Animation vente flash invalide");
    }
  })
);

export default flashSaleAnimationRouter;
