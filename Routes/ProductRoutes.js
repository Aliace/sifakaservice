import express from "express";
import asyncHandler from "express-async-handler";
import { admin, protect } from "../Middleware/AuthMiddleware.js";
import Product from "./../Models/ProductModel.js";

const productRoute = express.Router();

// Get all products User
productRoute.get(
  "/list",
  asyncHandler(async (req, res) => {
    const pageSize = 6;
    const page = Number(req.query.pageNumber) || 1;
    const parentId = req.query.parentId;
    const searchText = req.query.searchText
      ? {
          name: {
            $regex: req.query.searchText,
            $options: "i",
          },
        }
      : {};
    const count = await Product.countDocuments({ ...searchText });
    const products = await Product.find({
      ...searchText,
      ...(parentId ? { parentId } : {}),
    })
      .limit(pageSize)
      .skip(pageSize * (page - 1))
      .sort({ _id: -1 });
    res.json({ products, page, pages: Math.ceil(count / pageSize) });
  })
);

// ADMIN GET ALL PRODUCT
productRoute.get(
  "/all",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const pageSize = 8;
    const page = Number(req.query.pageNumber) || 1;
    const parentId = req.query.parentId;
    const searchText = req.query.searchText
      ? {
          name: {
            $regex: req.query.searchText,
            $options: "i",
          },
        }
      : {};
    const count = await Product.countDocuments({ ...searchText });
    const products = await Product.find({
      ...searchText,
      ...(parentId ? { parentId } : {}),
    })
      .limit(pageSize)
      .skip(pageSize * (page - 1))
      .sort({ _id: -1 });
    // console.log('*****PROd*****',products)
    res.json({ products, page, pages: Math.ceil(count / pageSize) });
    // const products = await Product.find({}).sort({ _id: -1 });
    // res.json(products);
  })
);

// Get single product
productRoute.get(
  "/:id",
  asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id);
    if (product) {
      res.json(product);
    } else {
      res.status(404);
      throw new Error("Produits invalide");
    }
  })
);

// Product Review
productRoute.post(
  "/:id/review",
  protect,
  asyncHandler(async (req, res) => {
    const { rating, comment } = req.body;
    const product = await Product.findById(req.params.id);

    if (product) {
      const alreadyReviewed = product?.reviews.find(
        (view) => view?.user?.toString() === req?.user._id.toString()
      );
      if (alreadyReviewed) {
        res.status(400);
        throw new Error("Produit déjà révisé");
      }
      const review = {
        name: req.user.name,
        rating: Number(rating),
        comment,
        user: req.user._id,
      };

      product.reviews.push(review);
      product.numReviews = product.reviews.length;
      product.rating =
        product.reviews.reduce((acc, item) => item.rating + acc, 0) /
        product.reviews.length;

      await product.save();
      res.status(201).json({ message: "Note ajouté" });
    } else {
      res.status(404);
      throw new Error("Produits invalide");
    }
  })
);

// Delete product
productRoute.delete(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id);
    if (product) {
      await product.remove();
      res.json({ message: "Produit supprimé" });
    } else {
      res.status(404);
      throw new Error("Produits invalide");
    }
  })
);

// Create product
productRoute.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    // console.log('DATA', req.body)
    const { name, image, description, price, countInStock, parentId, ref } =
      req.body;
    const productExist = await Product.findOne({ name });
    if (productExist) {
      res.status(404);
      throw new Error("Nom de produit déjà existe");
    } else {
      const product = new Product({
        name,
        image,
        description,
        price,
        countInStock,
        parentId,
        user: req.user._id,
        ref,
      });
      if (product) {
        const createdProduct = await product.save();
        res.status(201).json(createdProduct);
      } else {
        res.status(400);
        throw new Error("Données invalide");
      }
    }
  })
);

// Edit product
productRoute.put(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { name, image, description, price, countInStock, parentId, ref } =
      req.body;
    const product = await Product.findById(req.params.id);
    if (product) {
      product.name = name || product.name;
      product.image = image || product.image;
      product.description = description || product.description;
      product.price = price || product.price;
      product.countInStock = countInStock || product.countInStock;
      product.parentId = parentId || product.parentId;
      product.ref = ref || product.ref;

      const updatedProduct = await product.save();
      res.json(updatedProduct);
    } else {
      res.status(404);
      throw new Error("Produit non trouvé");
    }
  })
);

export default productRoute;
