import express from "express";
import asyncHandler from "express-async-handler";
import path from "path";
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);

const downloadRouter = express.Router();
const __dirname = path.dirname(__filename);

downloadRouter.get(
  `/:fileName`,
  asyncHandler(async (req, res) => {
    const fileName = req.params?.fileName;
    const jsonPath = path.join(__dirname, "../uploads", fileName);
    try {
      res.download(jsonPath);
    } catch (err) {
      console.error(err);
    }
  })
);

export default downloadRouter;
