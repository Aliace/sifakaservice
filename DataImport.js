import express from "express";
import User from "./Models/UserModel.js";
import users from "./data/users.js";
import Product from "./Models/ProductModel.js";
import products from "./data/Products.js";
import asyncHandler from "express-async-handler";
import Category from "./Models/Category.js";
import categories from "./data/Categories.js";
import FlashSale from "./Models/FlashSaleModel.js";
import flashsales from "./data/FlashSale.js";

const ImportData = express.Router();

ImportData.post(
  "/user",
  asyncHandler(async (req, res) => {
    await User.remove({});
    const importUser = await User.insertMany(users);
    res.send({ importUser });
  })
);

ImportData.post(
  "/products",
  asyncHandler(async (req, res) => {
    await Product.remove({});
    const importProducts = await Product.insertMany(products);
    res.send({ importProducts });
  })
);

ImportData.post(
  "/categories",
  asyncHandler(async (req, res) => {
    await Category.remove({});
    const importCategorys = await Category.insertMany(categories);
    res.send({ importCategorys });
  })
);

ImportData.post(
  "/venteflashs",
  asyncHandler(async (req, res) => {
    await FlashSale.remove({});
    const importFlashsales = await FlashSale.insertMany(flashsales);
    res.send({ importFlashsales });
  })
);

export default ImportData;
