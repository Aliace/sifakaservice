const categories = [
  {
    name: "Works Hard",
    image: "https://preview.ibb.co/fuL4BS/learn_fast.jpg",
    description: "I don't give up until I've figured it out",
  },
  {
    name: "Awesome Skills",
    image: "https://preview.ibb.co/fuL4BS/learn_fast.jpg",
    description: "I don't give up until I've figured it out",
  },
  {
    name: "Fast Learner",
    image: "https://preview.ibb.co/fuL4BS/learn_fast.jpg",
    description: "I don't give up until I've figured it out",
  },
];

export default categories;
