import express from 'express';
import dotenv from 'dotenv';
import connectDataBase from './config/MongoDB.js';
import ImportData from './DataImport.js';
import productRoute from './Routes/ProductRoutes.js';
import { errorHandler, notFound } from './Middleware/Error.js';
import userRouter from './Routes/UserRoutes.js';
import orderRouter from './Routes/OrderRoutes.js';
import categoryRouter from './Routes/Category.js';
import flashsaleRoute from './Routes/FlashSaleRoutes.js';
import subCategoryRouter from './Routes/SubCategoryRoutes.js';
import imagesRouter from './Routes/ImageRoutes.js';
import downloadRouter from './Routes/DownloadRoutes.js';
import cors from 'cors';
import sifakaAnimationRouter from './Routes/SifakaAnimationRouter.js';
import flashSaleAnimationRouter from './Routes/FlashSaleAnimationRouter.js';

dotenv.config();
connectDataBase();
const app = express();
app.use(express.json());

const corsOptions = {
  origin: '*',
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

// API
app.use('/api/import', ImportData);
app.use('/api/products', productRoute);
app.use('/api/users', userRouter);
app.use('/api/orders', orderRouter);
app.use('/api/category', categoryRouter);
app.use('/api/subcategory', subCategoryRouter);
app.use('/api/venteflashs', flashsaleRoute);
app.use('/api/upload', imagesRouter);
app.use('/api/download', downloadRouter);
app.use('/api/animationFlashSale', flashSaleAnimationRouter);
app.use('/api/animationSifaka', sifakaAnimationRouter);

// Error handler
app.use(notFound);
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Serveur démmare sur le port ${PORT}`));
