import mongoose from "mongoose";

const ImageSchema = mongoose.Schema({
  image: {
    data: Buffer,
    contentType: String,
  },
});

const UploadImage = mongoose.model("UploadImage", ImageSchema);

export default UploadImage;
