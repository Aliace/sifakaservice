import mongoose from "mongoose";

const flashSaleAnimationSchema = mongoose.Schema({
  animation: {
    type: String,
  },
});

const FlashSaleAnimation = mongoose.model(
  "FlashSaleAnimation",
  flashSaleAnimationSchema
);

export default FlashSaleAnimation;
