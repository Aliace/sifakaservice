import mongoose from "mongoose";

const sifakaAnimationSchema = mongoose.Schema({
  animation: {
    type: String,
  },
});

const SifakaAnimation = mongoose.model(
  "SifakaAnimation",
  sifakaAnimationSchema
);

export default SifakaAnimation;
